From: Maximiliano Sandoval <msandova@gnome.org>
Date: Sun, 28 Jan 2024 18:29:16 +0100
Subject: camera: Remove portal usage

At the moment there are three bugs that make this not worth the effort:

 - https://github.com/flatpak/xdg-dbus-proxy/issues/46: Sometimes the
 portal will just fail for no good reason making us fallback to the
 non-sandboxed method.
 - When opening the app for the first time the dialog that asks
 for camera permissions is not shown, forcing a big number of users to
 use Flatseal.
 - When not using flatpak (needs confirmation), the portal call will
 succeed and return a file descriptor, but such file descriptor does
 contain access to the cameras. Since neither the portal or PipeWire
 report any problem this will lockout the user from using the app.

We have been not able to reproduce the second and third error.

(cherry picked from commit 608a8b6ac1f2870fba285cb3e286c8eb62c81146)

Origin: https://gitlab.gnome.org/GNOME/snapshot/-/merge_requests/194
---
 Cargo.toml            |  1 -
 src/widgets/camera.rs | 26 --------------------------
 2 files changed, 27 deletions(-)

--- a/Cargo.toml
+++ b/Cargo.toml
@@ -12,7 +12,6 @@
 [dependencies]
 adw = { package = "libadwaita", version = "0.6", features = ["v1_5"] }
 anyhow = "1.0"
-ashpd = { version = "0.8", features = ["gtk4", "tracing"] }
 futures-channel = "0.3.26"
 gettext-rs = { version = "0.7", features = ["gettext-system"] }
 gst = { package = "gstreamer", version = "0.22", features = ["v1_20"] }
--- a/src/widgets/camera.rs
+++ b/src/widgets/camera.rs
@@ -1,9 +1,6 @@
 // SPDX-License-Identifier: GPL-3.0-or-later
-use std::os::unix::io::OwnedFd;
-
 use adw::prelude::*;
 use adw::subclass::prelude::*;
-use ashpd::desktop::camera;
 use gettextrs::gettext;
 use gtk::CompositeTemplate;
 use gtk::{gio, glib};
@@ -213,22 +210,6 @@
 
         glib::spawn_future_local(
             glib::clone!(@weak self as obj, @strong provider => async move {
-                match stream().await {
-                    Ok(fd) => {
-                        if let Err(err) = provider.set_fd(fd) {
-                            log::error!("Could not use the camera portal: {err}");
-                        };
-                    }
-                    Err(ashpd::Error::Portal(ashpd::PortalError::NotAllowed(err))) => {
-                        // We don't start the device provider if we are not
-                        // allowed to use cameras.
-                        log::warn!("Permission to use the camera portal denied: {err}");
-                        obj.imp().permission_denied.set(true);
-                        obj.update_state();
-                        return;
-                    },
-                    Err(err) => log::warn!("Could not use the camera portal: {err}"),
-                }
                 if let Err(err) = provider.start_with_default(glib::clone!(@weak obj => @default-return false, move |camera| {
                     let stored_id = obj.imp().settings().string("last-camera-id");
                     !stored_id.is_empty() && id_from_pw(camera) == stored_id
@@ -523,13 +504,6 @@
     }
 }
 
-async fn stream() -> ashpd::Result<OwnedFd> {
-    let proxy = camera::Camera::new().await?;
-    proxy.request_access().await?;
-
-    proxy.open_pipe_wire_remote().await
-}
-
 // Id used to identify the last-used camera.
 fn id_from_pw(camera: &aperture::Camera) -> glib::GString {
     camera.display_name()
